# ⟁ Apple flyer msu to-do ⟁
- install script
  - nvm? (later)
  - fix brew path on M1
    - maybe run after
  - add M1 case to check for homebrew 
    - start of check
  - change `npm i` to `npm ci` (clean install)

-	run script
  - git updates?
  - launch node + safari correctly

- style changes
	- change font color
	- bullet point indentation
	- split views into multiple files
	- per-item discount calculation (using ?:)
  - add date to page title so pdf has name automatically

- README
  - edit readme pic to show landscape

- csv import
	- use | for color seperator, . for color code
