const express = require('express');
const app = express();
const fs = require('fs/promises')

const port = 3333;
const host = '0.0.0.0';

const { exec } = require('child_process');

let openSafari = () => exec(`open -a 'Safari' "http://localhost:${port}/"`, (err, stdout, stderr) => {
  if (err) { console.error(`error opening Safari: ${err}`); return }
})

if (process.argv[2] && process.argv[2] === 'dev') {
  console.log('running in dev mode, Safari will not automatically open')
} else {
  openSafari()
}

let chunk = (arr, chunkSize) => {
  if (chunkSize <= 0) throw "Invalid chunk size";
  var R = [];
  for (var i=0,len=arr.length; i<len; i+=chunkSize)
      R.push(arr.slice(i,i+chunkSize));
  return R;
}

let macs;
let ipads;
let config;
(async ()=>{
  // read mac data from json file
  const macjson = await fs.readFile('./macs.json')  
  macs = JSON.parse(macjson)

  // chunk colors in groups of two
  for (n of macs) n.colorsChunked = chunk(n.colors, 2)


  // read ipad data from json file
  const ipadjson = await fs.readFile('./ipads.json')  
  ipads = JSON.parse(ipadjson)  
  
  for (n of ipads) n.colorsChunked = chunk(n.colors, 2)

  // config
  // this is where $20 off AC+ will apply
  // left open for further expansion
  const cfgjson = await fs.readFile('./config.json')  
  config = JSON.parse(cfgjson)
  // console.log(config)
})();

app.set('views', './views');
app.set('view engine', 'pug');

app.get('/', (req, res) => {
  res.render('index', {
    macs: macs,
    ipads: ipads,
    twentyOff: config.twentyOff
  });
});

app.use(express.static('public'));

app.listen(port, host, () => {
	console.log(`Server started at ${host} port ${port}`);
  console.log(`http://${host}:${port}`)
});
