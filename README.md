# README (note: WIP!)
THIS SOFTWARE WILL ONLY WORK ON macOS Safari

note: uses port 3333 on localhost


## Installation
yes, this does look hard but trust me it's not. just copy and paste the commands exactly and
you'll be fine 😃

1. open up terminal. all commands will be enclosed `like this`. just copy and paste line by line
2. install xcode command line tools `xcode-select --install`
3. install Homebrew `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
4. copy and run the two lines that homebrew asks you to
5. run `exec zsh -l` (this reloads terminal so it can find brew and node)
6. install nodejs `brew install nodejs`
7. clone repo and cd to it `cd ~/Documents; git clone https://gitlab.com/ryanknutson/apple-flyer-msu; cd apple-flyer-msu`
8. run `npm i` to install deps

repo will be stored under your Documents folder


## Starting
1. open Terminal and navigate to the repo
   (run `cd ~/Documents/apple-flyer-msu`)
2. run `npm start`
3. Safari will automatically open. If it says 'Safari Can't Connect to the server', try refreshing (⌘-R)

if for whatever reason Safari didn't open, launch it yourself and go to
`http://localhost:3333`


## Printing
1. press ⌘-P to open print dialog
2. **important!** make sure that 'landscape' and 'Print backgrounds' are
   selected  
   ![](READMEpics/printdialog.png)
3. at the bottom of the page, hit 'PDF'. save your PDF in an easily accessable
   area
4. open the created PDF in Preview
5. click on page 2 to select the 2nd page
6. click "rotate" twice so that the 2nd page appears upside down (this is what
   you want!)
   ![](READMEpics/androtate.png)
7. hit ⌘+S to save the PDF (or menu bar -> file -> save)
8. done! 💯 you can now print off your PDF or send it to a print shop


## Dev mode
to prevent Safari from automatically opening, and to compile the SASS, run
`npm run dev` to start the server in dev mode
